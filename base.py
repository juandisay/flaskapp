
__author__= 'juandisay'



#flask app
from flask import (Flask, url_for, render_template, redirect, request, flash,
	jsonify, abort, Blueprint, make_response
	)


#ext flask db 
from flask_sqlalchemy import SQLAlchemy, BaseQuery

#security hashing for hash password
from werkzeug.security import generate_password_hash, check_password_hash

#ext flask login
from flask_login import (LoginManager,UserMixin,
	login_required,login_user,logout_user, current_user
	)

from flask_restful import Api, Resource, fields, marshal_with

#ext flask admin
from flask_admin import Admin, AdminIndexView, expose, helpers

#ext flask manager/script
from flask_script import Server, Shell, Manager, prompt_bool

#ext flask form and wtforms
from flask_wtf import FlaskForm
from wtforms.validators import InputRequired, Email, Length, DataRequired
from wtforms.fields import (BooleanField, DateField, DateTimeField, DecimalField
	, HiddenField, IntegerField, PasswordField, RadioField, SelectField,
	SelectMultipleField, StringField, SubmitField, TextAreaField, TextField
	)

from flask_migrate import Migrate, MigrateCommand

#Config Flask
app=Flask(__name__)


#Config extensi flask_script 
manage = Manager(app)

#Config extensi Flask_login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view='admin.login'

#Config extensi Flask_Sqlalchemy
db = SQLAlchemy(app)

migrate = Migrate(app, db)


#configuration via object
app.config.from_pyfile("config.cfg")

#list config db copas to config.cfg
#SQLALCHEMY_DATABASE_URI = "mysql+mysqldb://%s:%s@%s/%s" % ('root', 'admin', '127.0.0.1', 'task_m')
# SQLALCHEMY_DATABASE_URI = 'sqlite:///task_m.db'

# SQLALCHEMY_DATABASE_URI = "mysql+mysqldb://%s:%s@%s/%s" % ('root', 'admin', '127.0.0.1', 'task_db')
# SQLALCHEMY_DATABASE_URI = "mysql+mysqldb://sql12203262:Ijdb4LwrXq@sql12.freemysqlhosting.net:3306/sql12203262"