import os
import time

import exceptions

from ebaysdk.trading import Connection as Trading
from ebaysdk.exception import ConnectionError
from ebaysdk.utils import getNodeText
from dateutil.parser import parse
from models import db,EbayOrder
from sqlalchemy.exc import IntegrityError, InvalidRequestError


# set path
ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
pro_ebay = os.path.join(ROOT_PATH, "ebay.yaml")
api = Trading(config_file=pro_ebay)

result=[]

class TradingEbay:

    def getorders(self):
        try:
            getorders = api.execute('GetOrders', {'NumberOfDays': 30})
            return getorders.dict()
        except ConnectionError as e:
            return e
            return e.getorders.dict()

    def getuser(self):
        try:
            getuser = api.execute('GetUser', {})
            return getuser.dict()
        except ConnectionError as e:
            return e.getuser.dict()


class DataLoaderEbay(TradingEbay):

	
	def __init__(self):
		self.order=self.getorders()
		self.user=self.getuser()
		self.takeorder()

	def takeorder(self):
		address=["Name","Street1","Street2","CountryName","StateOrProvince","Country","Phone","PostalCode","AddressID"]
		split=lambda a,b : b if a in b and b is 'null' else a
		datetime=lambda x:parse(x).strftime("%d-%m-%Y %H:%M")
		ShippingAddress=lambda x:",".join(str(a) for a in x)
		for keys in self.order:
			if keys == "OrderArray":
				for filterdata in self.order[keys]["Order"]:
					self.savetodb(Ebay_account=self.user["User"]["UserID"],
						Source=self.user["User"]["SellerInfo"]["StoreURL"],
						ItemID=filterdata["TransactionArray"]
						["Transaction"][0]["Item"]["ItemID"],
						Item=filterdata["TransactionArray"]["Transaction"][0]
						["Item"]["Title"],
						Buyer_Email=filterdata["TransactionArray"]["Transaction"][0]
						["Buyer"]["Email"],
						Buyer=filterdata["BuyerUserID"],
						Shipping_Address=ShippingAddress([ x for x in [
						filterdata["ShippingAddress"][split(x,y)] for x,y in zip(address,filterdata["ShippingAddress"])]]),
						Price=filterdata["Total"]["value"],
						Quantity=filterdata["TransactionArray"]["Transaction"][0]
						["QuantityPurchased"],
						Sold_Out=datetime(filterdata["CreatedTime"]))		
	
	def savetodb(self,**kwargs):
		data=kwargs
		result.append(data)