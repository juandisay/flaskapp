from flask import Flask
from flask_script import Server, Shell, Manager, prompt_bool
from flask_migrate import Migrate, MigrateCommand
from apps.eccomerce.controllers import ebayAPI

#configurasi and add Extensi
app=Flask(__name__)
app.config.from_pyfile("config.cfg")
manage=Manager(app)

#Register Command
manage.add_command("db", MigrateCommand)

#Register Blueprint
app.register_blueprint(ebayAPI)

if __name__=="__main__":
	app.run()