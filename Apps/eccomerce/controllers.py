from flask import Blueprint, jsonify
from handlerEbaytrading import Trading


ebayAPI=Blueprint("ebayAPI",__name__,
	static_folder="asset-ebay",
	template_folder="template-ebay")

trade=Trading()

@ebayAPI.route("/")
def hello():
	return jsonify({"message":"its work"})

@ebayAPI.route("/getorder")
def getorder():
	return jsonify(trade.getorder())

@ebayAPI.route("/getorders")
def getorders():
	return jsonify(trade.getorders())


@ebayAPI.route("/getuser")
def getuser():
	return jsonify(trade.getuser())
