import os
import urllib2

from ebaysdk.trading import Connection as Trading
from ebaysdk.exception import ConnectionError
from ebaysdk.utils import getNodeText

#set path
ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
pro_ebay = os.path.join(ROOT_PATH, "ebay.yaml")
api = Trading(config_file=pro_ebay)


class Trading:

	def getorders(self):
		try:
			getorders = api.execute('GetOrders', {'NumberOfDays': 30})
			return getorders.dict()
		except ConnectionError as e:
			return e
			return e.getorders.dict()

	def getuser(self):
		try:
			getuser = api.execute('GetUser', {})
			return getuser.dict()
		except ConnectionError as e:
			return e
			return e.getuser.dict()
